FROM alpine:latest
MAINTAINER Nguyen Cap Tien. <nguyen@crystal-techs.com>

# Add repos
RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories

RUN apk update && apk add bash apache2 php7-apache2 curl ca-certificates tar zip \
  php7-zip php7-zlib php7-bz2 php7-json php7-xml php7-dom php7-xmlreader php7-xmlrpc php7-gettext php7-soap php7-xsl\
  php7-pdo php7-pdo_mysql php7-pdo_pgsql php7-pdo_sqlite php7-pdo_dblib php7-mysqli php7-sqlite3 php7-odbc php7-redis \
  php7-phar php7-openssl php7-session php7-curl php7-ctype \
  php7-gd php7-iconv php7-mcrypt php7-bcmath php7-iconv php7-curl php7-intl php7-mbstring \
  php7-opcache php7-apcu \
  mysql-client
  
RUN ln -s /usr/bin/php7 /usr/bin/php && rm -f /var/cache/apk/*

ADD default/ /default
ADD run.sh /
RUN chmod +x /run.sh

EXPOSE 80
VOLUME ["/DATA"]

CMD ["/run.sh"]