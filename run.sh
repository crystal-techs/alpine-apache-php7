#!/bin/sh

cp -Ru /default/* /DATA
chown -Rf apache:apache /DATA/public_html

# start nginx
mkdir -p /run/apache2
chown apache:apache /run/apache2
chown -Rf apache:apache /DATA/logs/
cp -rf /DATA/config/php.ini /etc/php7/php.ini
cp -rf /DATA/config/httpd.conf /etc/apache2/httpd.conf
cp -rf /DATA/config/conf.d/* /etc/apache2/conf.d/

rm -f /run/apache2/apache2.pid
rm -f /run/apache2/httpd.pid

echo "Starting apache..."
httpd -D FOREGROUND